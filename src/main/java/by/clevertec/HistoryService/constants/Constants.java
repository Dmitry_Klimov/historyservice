package by.clevertec.HistoryService.constants;

public class Constants {
    public static final String HISTORY = "/history";
    public static final String PAGESIZE_KEY = "pagesize";
    public static final String PAGESIZE_VALUE = "15";
    public static final String PAGENUMBER_KEY = "pagenumber";
    public static final String PAGENUMBER_VALUE = "0";
    public static final String ID_PATH_VARIABLE = "/{id}";
    public static final String HISTORY_SAVED = "History saved successful : {}";
    public static final String HISTORY_RECEIVED = "History with id={} received successful : {}";
    public static final String HISTORY_PAGEABLE = "History pageable received successful : {}";
    public static final String ERROR_PARSING_OF_OBJECT = "can't represent object of class {} in json form for logging: {}";
}